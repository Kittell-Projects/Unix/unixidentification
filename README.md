# Unix Identification

Unix Identification is a script that originated with VMWare but I have added some other systems to the script.

## Simple Unix Distribution Check
Running this command will ask you what kind of details you want to receive

    /bin/bash -c "$(curl -fsSL https://gitlab.com/Kittell-Projects/Unix/unixidentification/-/raw/master/UnixIdentification.sh)"

**Example:**

    /bin/bash -c "$(curl -fsSL https://gitlab.com/Kittell-Projects/Unix/unixidentification/-/raw/master/UnixIdentification.sh)"
    What details would you like to see?
    -a is for all details.
    -d is for full OS name with version.
    -r is for the release version of the OS
    -i is for the name of the distribution
     
    Type your option (a, d, r, or i), followed by [ENTER]:

Type the appropriate letter then tap the Enter/Return key

*   a
    *   CentOS  
        8.3.2011  
        CentOS Linux release 8.3.2011
*   d
    *   CentOS Linux release 8.3.2011
*   r
    *   8.3.2011
*   i
    *   CentOS
